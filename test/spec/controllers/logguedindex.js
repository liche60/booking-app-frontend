'use strict';

describe('Controller: LogguedindexCtrl', function () {

  // load the controller's module
  beforeEach(module('bookingAppFrontendApp'));

  var LogguedindexCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LogguedindexCtrl = $controller('LogguedindexCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LogguedindexCtrl.awesomeThings.length).toBe(3);
  });
});
