'use strict';

describe('Directive: sessionHeader', function () {

  // load the directive's module
  beforeEach(module('bookingAppFrontendApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<session-header></session-header>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the sessionHeader directive');
  }));
});
