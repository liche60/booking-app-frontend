'use strict';

describe('Service: airportSirvice', function () {

  // load the service's module
  beforeEach(module('bookingAppFrontendApp'));

  // instantiate service
  var airportSirvice;
  beforeEach(inject(function (_airportSirvice_) {
    airportSirvice = _airportSirvice_;
  }));

  it('should do something', function () {
    expect(!!airportSirvice).toBe(true);
  });

});
