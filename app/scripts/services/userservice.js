'use strict';

/**
 * @ngdoc service
 * @name bookingAppFrontendApp.UserService
 * @description
 * # UserService
 * Service in the bookingAppFrontendApp.
 */
angular.module('bookingAppFrontendApp').service('UserService', ['$http', 'ENV', '$auth', '$location', '$state', 'Restangular', '$q',
  function($http, ENV, $auth, $location, $state, Restangular, $q) {
        
    var self = this;

    this.authenticatedObservable = new Rx.BehaviorSubject();
    this.authenticatedObservable.onNext($auth.isAuthenticated());

    var onSuccessfulLogin = function (redirectTo, goTo) {
      self.authenticatedObservable.onNext(true);
      if(redirectTo) {
        $location.url(redirectTo);
      } else if(goTo){
        $state.go(goTo);
      } else {
        $state.go('main');
      }
    };

    this.authenticateUsingEmail = function(email, password, redirectTo, tokenBoot, goTo){
      return $auth.login({ email: email, password: password, rememberMe: false})
        .then(function() {
          onSuccessfulLogin(redirectTo, goTo);
        });
    };

    this.getUser = function() {
        return $http.get(ENV.apiEndpoint + '/user').then(function (response) {
            return response.data;
        });
    };

    this.logout = function() {
        return $auth.logout().finally(function(){
            self.authenticatedObservable.onNext(false);
            self.deleteUserInfo();
        });
    };

    this.isAuthenticated = function () {
        return $auth.isAuthenticated();
    };

    this.signUp = function (data) {
      return $auth.signup(data);
    };

}]);
