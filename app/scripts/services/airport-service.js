'use strict';

/**
 * @ngdoc service
 * @name bookingAppFrontendApp.airportSirvice
 * @description
 * # airportSirvice
 * Service in the bookingAppFrontendApp.
 */
angular.module('bookingAppFrontendApp').service('AirportService', ['$q', 'Restangular', function ($q, Restangular) {
	
	var Airports = Restangular.all('airports');

	this.getAirports = function(query){
		if(query && query.length() < 3){
			return Promise.resolve([]);
		}
        return Airports.all(query).getList().then(function (airports) {
            return airports;
        });
            
    };

}]);
