'use strict';

/**
 * @ngdoc directive
 * @name bookingAppFrontendApp.directive:scheduleFinder
 * @description
 * # scheduleFinder
 */
angular.module('bookingAppFrontendApp')
  .directive('scheduleFinder', function () {
    return {
      templateUrl: 'views/schedule-finder.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });
