'use strict';

/**
 * @ngdoc function
 * @name bookingAppFrontendApp.controller:SigninCtrl
 * @description
 * # SigninCtrl
 * Controller of the bookingAppFrontendApp
 */
angular.module('bookingAppFrontendApp')
  .controller('SigninCtrl', ['$scope', '$stateParams', '$state', 'UserService', function ($scope, $stateParams, $state, UserService){

    $scope.authenticate = function() {
        $scope.error = null;
        var email = $scope.email;
        var password = $scope.password;
        UserService.authenticateUsingEmail(email, password, $stateParams.redirectTo)
        .then(function(response){
            $scope.error = null;
        }, function(error){
            $scope.error = "Autentication error. Please try again!";
        });
    };

    $scope.providers = [{id: "google"}];
    
}]);
