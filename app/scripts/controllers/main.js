'use strict';

/**
 * @ngdoc function
 * @name bookingAppFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bookingAppFrontendApp
 */
angular.module('bookingAppFrontendApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
