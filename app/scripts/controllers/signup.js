'use strict';

/**
 * @ngdoc function
 * @name bookingAppFrontendApp.controller:SignupCtrl
 * @description
 * # SignupCtrl
 * Controller of the bookingAppFrontendApp
 */
angular.module('bookingAppFrontendApp').controller('SignupCtrl', ['$scope', '$stateParams', '$state', 'UserService', function ($scope, $stateParams, $state, UserService){
	
	$scope.signUp = function () {
		$scope.error = null;
		UserService.signUp($scope.signUpData, $stateParams.redirectTo).then(
		 	function () {
				$scope.error = null;
				if(!$stateParams.redirectTo){
					$state.go('main.index');
				}
		 	}, 
			function (err) {
				if(err && err.data){
					$scope.error = err.data;
				} else {
					$scope.error = "User can´t be register, please, try again!";
					console.log(err);
				}
			}
		);
	};

}]);
